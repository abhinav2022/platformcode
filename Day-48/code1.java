class Solution {
    public int firstMissingPositive(int[] nums) {
        Arrays.sort(nums);
        int cnt=1;
        for(int i=0;i<nums.length;i++){
            if(cnt==nums[i])
                cnt++;

        }
        return cnt;
    }
}