class Solution {
    public List<Integer> majorityElement(int[] nums) {
        ArrayList<Integer> al = new ArrayList<Integer>();
        int y=nums.length/3;

        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();

        for(int i=0;i<nums.length;i++){
            hm.put(nums[i],hm.getOrDefault(nums[i],0)+1);
        }

        for(Map.Entry mp:hm.entrySet()){
            if((int)mp.getValue()>y)
                al.add((int)mp.getKey());
            
        }

        return al;
    }
}