class Solution {
    public int[] findMissingAndRepeatedValues(int[][] grid) {
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        int arr[]=new int[2];
        int y=1;
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[i].length;j++){
                hm.put(grid[i][j],hm.getOrDefault(grid[i][j],0)+1);
            }
        }

        for(Map.Entry mp : hm.entrySet()){
            if((int)mp.getValue()==2)
                arr[0]=(int)mp.getKey();
            if((int)mp.getKey()==y)
                y++;
        }
        arr[1]=y;
        return arr;
    }
}