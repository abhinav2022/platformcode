class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int y=(nums1.length + nums2.length)/2;
        int x=y-1;
        //int arr[]=new int[nums1.length + nums2.length];
        int i=0,j=0,k=0,id1=0,id2=0;
        while(i<nums1.length && j<nums2.length){
            
            if(nums1[i]<nums2[j]){
                //arr1[k++]=nums1[i++];
                if(k==y)id1=nums1[i];
                if(k==y-1)id2=nums1[i];
                i++;
               
            }else{
                 if(k==y)id1=nums2[j];
                if(k==y-1)id2=nums2[j];
                j++;
                
               // arr1[k++]=nums2[j++];
            }
            k++;
        }

        while(i<nums1.length){
      
                     if(k==y)id1=nums1[i];
                if(k==y-1)id2=nums1[i];
                k++;
                i++;
            
        }
         while(j<nums2.length){
      
                     if(k==y)id1=nums2[j];
                if(k==y-1)id2=nums2[j];
                k++;
                j++;
            
        }
       
        if((nums1.length+nums2.length)%2==0){
            return ((double)(id1+id2)/2);
        }else{
            return id1;
        }
    }
}