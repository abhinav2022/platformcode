class Solution {
    public int[] rowAndMaximumOnes(int[][] mat) {
   
        int y=0,m=0,l=0;
        for(int i=0;i<mat.length;i++){
            y=0;
            for(int j=0;j<mat[i].length;j++){
                if(mat[i][j]==1){
                        y++;
                }
            }
            if(y>m){
                m=y;
                l=i;
            }
        }

        return new int[]{l,m};
    }
}