class Solution {
    public void sortColors(int[] arr) {
        int num0=0,num1=0,num2=0;

        for(int i=0;i<arr.length;i++){
            if(arr[i]==0)
                num0++;
            else if(arr[i]==1)
            num1++;
            else if(arr[i]==2)
                num2++;
        }

        for(int i=0;i<arr.length;i++){
            if(num0>0){
                arr[i]=0;
                num0--;
            }else if(num1>0){
                arr[i]=1;
                num1--;
            }else if(num2>0){
                arr[i]=2;
                num2--;
            }
        }
    }

}