
class Solution
{
    //Function to return a list of integers denoting spiral traversal of matrix.
    static ArrayList<Integer> spirallyTraverse(int matrix[][], int r, int c)
    {
          ArrayList<Integer> al = new ArrayList<Integer>();
        int N=matrix[0].length,i=0,j=0,m=matrix.length;
        int u=0,l=0;
       
        if(N==1){
           for(int x=1;x<=m;x++,j++){
                al.add(matrix[j][i]);
            }
            return al;
        }
        if(m==1){
             for(int x=1 ;x<=N;x++,i++){
                al.add(matrix[j][i]);
            }
            return al;
        }
        while(N>1 && m>1){
            
            for(int x=1 ;x<=N-1;x++,i++){
                al.add(matrix[j][i]);
            }
            for(int x=1;x<=m-1;x++,j++){
                al.add(matrix[j][i]);
            }
           for(int x=1;x<=N-1;x++,i--){
                al.add(matrix[j][i]);
           }
            for(int x=1;x<=m-1;x++,j--){
                al.add(matrix[j][i]);
            }

            N-=2;
            m-=2;
            i++;
            j++;
        }
        if(N>0 && m>0){
             for(int x=0 ;x<N-1;x++,i++){
                al.add(matrix[j][i]);
            }
            for(int x=0;x<m;x++,j++){
                al.add(matrix[j][i]);
            }
        }
        
        return al; 
    }
}
