class Solution {
    public static int maxProfit(int n, int[] price) {
        // code here
        int arr[] =new int[n];
        
        int max=price[n-1];
        
        for(int i=n-2;i>=0;i--){
            if(price[i]>max)
            max=price[i];
            
            arr[i]=Math.max(arr[i+1],max-price[i]);
        }
        
        int min=price[0];
        
        for(int i=1;i<n;i++){
            if(min>price[i]){
                min=price[i];
            }
            
            arr[i]=Math.max(arr[i-1],arr[i]+price[i]-min);
        }
        
        return arr[n-1];
    }
}
        
