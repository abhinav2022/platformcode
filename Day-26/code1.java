class Solution {
    public int findPairs(int[] nums, int k) {
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        
        for(int i=0;i<nums.length;i++){
            if(hm.containsKey(nums[i]))
                hm.put(nums[i],hm.get(nums[i])+1);
            else
                hm.put(nums[i],1);
        }
        int cnt=0;
        for(int mp: hm.keySet()){
           
            if(mp+k==mp){
                if((int)hm.get(mp)>1)
                    cnt++;
            }
            else if(hm.containsKey(mp+k))
                cnt++;
         
        }

        return cnt;
    }
}