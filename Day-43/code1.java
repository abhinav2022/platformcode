class Solution {
    public int maxScore(int[] cardPoints, int k) {
        int sum=0;

        for(int i=0;i<cardPoints.length;i++){
            sum+=cardPoints[i];
        }

        if(k==cardPoints.length){
            return sum;
        }
        int rr=0,m=0,i=0,j=0;

        for(;i<cardPoints.length;i++){
            rr+=cardPoints[i];

            if(i-j+1==cardPoints.length-k){
                m=Math.max(m,sum-rr);    
                rr-=cardPoints[j++];
            }
        }

        return m;
    }
}