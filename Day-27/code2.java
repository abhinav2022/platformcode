class Solution {
    public long kthElement( int arr1[], int arr2[], int n, int m, int k) {
        
        int i=0,j=0,l=0;
        long min=0;
        
        while(i<arr1.length && j<arr2.length && l<k){
            
            if(arr1[i]>arr2[j]){
                
                min=arr2[j];
                j++;
            }else{
                min=arr1[i];
                i++;
            }
            l++;
        }
        
        while(i<arr1.length && l<k){
            min=arr1[i];
            l++;
            i++;
        }
        
        while(j<arr2.length && l<k){
            min=arr2[j];
            l++;
            j++;
        }
        
        return min;
        
    }
}