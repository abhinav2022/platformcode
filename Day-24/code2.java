import java.util.* ;
import java.io.*; 
import java.util.ArrayList;

public class Solution {

  public static ArrayList < Double > findMedian(ArrayList < Integer > arr, int n, int m) {
    // Write your code here.
    ArrayList<Double> al = new ArrayList<Double>();

    int i=0,j=m-1,size=arr.size();
    if(m%2==0){

      while(j<size){
        //int y=(i+j)/2;
        int u=i;
        double arr2[]=new double[m];
        for(int l=0;l<m;l++){
          arr2[l]=arr.get(u++);
        }
        Arrays.sort(arr2);
        int y = (m-1)/2;
        double d = ((arr2[y]+arr2[y+1])/2);
        
        al.add(d);
        i++;
        j++;
      }
    }else{
      while(j<size){
        int y=(m)/2;
        int u=i;
         double arr2[]=new double[m];
        for(int l=0;l<m;l++){
          arr2[l]=arr.get(u++);
        }
        Arrays.sort(arr2);
        al.add(arr2[y]);
        i++;
        j++;
      }
    }

    return al;

  }
}