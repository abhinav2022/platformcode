class Solution {
    public int longestConsecutive(int[] nums) {
        SortedSet<Integer> ss = new TreeSet<Integer>();
        int cnt=1;
        if(nums.length==0)
            return 0;
            
        for(int i=0;i<nums.length;i++){
            ss.add(nums[i]);
        }

        Iterator itr = ss.iterator();
        int pp=1;
        while(itr.hasNext()){
            int p=(int)itr.next();

            if(ss.contains(p+1))
                pp++;
            else{
                cnt=Math.max(pp,cnt);
                pp=1;                
            }

        }
        return cnt;
    }
}