class Solution {
    public int leastInterval(char[] tasks, int n) {

    //     if(n==0)
    //         return tasks.length;
    //     HashMap<Character,Integer> hm = new HashMap<>();
    // int cnt=0;
    //     for(int i=0;i<tasks.length;i++){
    //         hm.put(tasks[i],hm.getOrDefault(tasks[i],0)+1);
    //     }
    //     // for(Map.Entry mp: hm.entrySet()){
    //     //     System.out.println(mp.getKey()+" "+mp.getValue());
    //     // }
    //    Iterator itr= hm.entrySet().iterator();
    //    itr2.next();
    //    while(!hm.isEmpty()){
    //         int k=n;
    //          Iterator itr2 = hm.entrySet().iterator();
    //          cnt++;
            
    //         // if(!itr2.hasNext()){
    //         //     itr2=hm.entrySet().iterator();           
    //         // }
                
    //         while(itr2.hasNext() && k>0){
    //             Map.Entry mp = (Map.Entry)itr2.next();
    //             char ch=(char)mp.getKey();
    //             int y=(int)mp.getValue();
    //             System.out.println(ch+" y "+y);
    //             if(y>1)
    //                 hm.put(ch,y-1);
    //             else if(y==1)
    //                 itr2.remove();
                
    //             k--;
    //             cnt++;
    //         }
    //          if(k>=0 && !hm.isEmpty())
    //           cnt+=k+1;
    //           System.out.println("cnt "+cnt);
    //     }

    //         return cnt;
    int[] taskCount = new int[26];

        // Count the frequency of each task
        for (char task : tasks) {
            taskCount[task - 'A']++;
        }

        // Sort the taskCount array in descending order
        Arrays.sort(taskCount);
        int maxCount = taskCount[25];

        // Count the number of tasks with the maximum frequency
        int maxTasksCount = (maxCount - 1) * n;

        // Iterate over the taskCount array and add tasks with lower frequency
        for (int i = 24; i >= 0 && taskCount[i] > 0; i--) {
            maxTasksCount -= Math.min(taskCount[i], maxCount - 1);
        }

        // Calculate the total number of slots required
        int totalSlots = tasks.length + Math.max(0, maxTasksCount);

        // Return the maximum of the totalSlots and the length of tasks array
        return Math.max(totalSlots, tasks.length);
    }
}