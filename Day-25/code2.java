class Solution {
    // public boolean exist(char[][] board, String word) {
        
    //    // boolean flag=false;
       
    //     for(int i=0;i<board.length;i++){
    //         for(int j=0;j<board[0].length;j++){
    //             if(board[i][j]==word.charAt(0) &&fun(board,i,j,0,word)){
    //                 // char p[][]=new char[board.length][board[0].length];
    //                 //boolean p[][]=new boolean[board.length][board[0].length];
    //                return true;
    //             }
                
    //         }
    //     }
    //     return false;
    // }

    // public boolean fun(char[][] arr,int i,int j,int n,String s){
    //     int y=s.length();
    //        if(n==y-1 )
    //         return true;

    //     // if(i<0 ||j<0 || i>=arr.length||j>=arr[i].length || arr[i][j]!=s.charAt(n+1))
    //     //     return false;
            
    //       char temp=arr[i][j];
    //       arr[i][j]='#';

    //     if(i <0 && arr[i-1][j]==s.charAt(n+1) && fun(arr,i-1,j,n+1,s))
    //         return true;
        
    //     if(j<0&&arr[i][j-1]==s.charAt(n+1) && fun(arr,i,j-1,n+1,s))
    //         return true;
        
    //     if(i<arr.length-1 && y-arr[i+1][j]==s.charAt(n+1) &&fun(arr,i+1,j,n+1,s))
    //         return true;

    //     if(j<arr[0].length-1 && arr[i][j+1]==s.charAt(n+1) && fun(arr,i,j+1,n+1,s))
    //         return true;
        
        
    //     arr[i][j]=temp;

    //     return false;
    // }
        public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;

        // Iterate through each cell in the board
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if ( fun(board, word, i, j, 0)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean fun(char[][] board, String word, int i, int j, int index) {
        // Check if we have reached the end of the word
        if (index == word.length()) {
            return true;
        }

        // Check if the current cell is out of bounds or does not match the current character
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length || board[i][j] != word.charAt(index)) {
            return false;
        }

        // Mark the current cell as visited
        char temp = board[i][j];
        board[i][j] = '*';

        // Recursively search in all four directions
        boolean found = fun(board, word, i + 1, j, index + 1) || fun(board, word, i - 1, j, index + 1) ||  fun(board, word, i, j - 1, index + 1) ||fun(board, word, i, j + 1, index + 1);

        // Restore the original value of the current cell
        board[i][j] = temp;

        return found;
    }

}