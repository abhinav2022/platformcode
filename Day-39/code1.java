class Solution {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        int flag=0;
        
        HashSet<List<Integer>> hs = new HashSet<List<Integer>>();
        for(int i=0;i<nums.length-3;i++){
            for(int j=i+1;j<nums.length-2;j++){
                int k=j+1,l=nums.length-1;
                while(k<l){
                    long u =(long)nums[i]+nums[j]+nums[k]+nums[l];
                    if(u==target){
                        hs.add(Arrays.asList(nums[i],nums[j],nums[k],nums[l]));
                        k++;
                        l--;
                        flag=1;
                    }else if(u<target){
                        k++;
                    }else
                    l--;
                }
            }
        }

        if(flag==1)
        return new ArrayList<>(hs);
        else 
            return new ArrayList<>();
    }
}