import java.util.*;
public class Solution {
	public static int maximizeScore(ArrayList<Integer> arr, int n, int k) {
		//Write your code here
		// int y=0,i=0,j=arr.size()-1,m=0;

		// if(k==arr.size()){
		// 	for(int o=0;o<=j;o++){
		// 		y+=arr.get(o);
		// 	}

		// 	return y;
		// }
		// while( m <k){
		// 	j=arr.size()-1;
		// 	int f=arr.get(0),l=arr.get(j);
		// 	if(f<l){
		// 		y+=l;
		// 		arr.remove(j);
				
		// 	}else{
		// 		y+=f;
		// 		arr.remove(0);
		// 	}
		// 	m++;
		// }

		// return y;
		int sum = 0;

		for(int num: arr){

			sum += num;

		}

		if(k == n) return sum;

		int i = 0, j = 0;

		int ref = 0;

		int ans = 0;

		while(i < n){

			ref += arr.get(i);

		if(i-j+1 == n-k){

			ans = Math.max(ans, sum-ref);

			ref -= arr.get(j);

			j++;

			}

			i++;

		}

		return ans;

	}
}

