class Solution {
    public int jump(int[] nums) {
        int z=0;
        int y=0;
        int max=0;
    if(nums.length==1)
        return 0;

        for(int i=0;i<nums.length;i++){
            max=Math.max(max,i+nums[i]);
            if(max>=nums.length-1){
                z++;
                break;
            }
            if(i==y){
                ++z;
                y=max;
            }
        }

        return z;
    }
}