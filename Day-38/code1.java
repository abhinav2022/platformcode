class Solution {
    public boolean canJump(int[] nums) {
        int y=0;

        for(int i=0;i<nums.length;i++){
            if(i>y)
                return false;
            
            y=Math.max(y,i+nums[i]);
        }

        return true;
    }
}