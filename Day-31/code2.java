class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        if(nums==null || nums.length<3) return new ArrayList<>();
        Arrays.sort(nums);
        Set <List<Integer>> hs = new HashSet<>();
        for(int i=0;i<nums.length-2;i++){
            int p=i+1,j=nums.length-1;
            while(p<j){

                int sum = nums[i]+nums[p]+nums[j];
                if(sum==0){
                    hs.add(Arrays.asList(nums[i],nums[p],nums[j]));
                    p++;
                    j--;
                }else if(sum<0)
                    p++;
                else
                j--;

            }
        }
        return new ArrayList<>(hs);
    }
}