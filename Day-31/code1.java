class Solution {
    public int threeSumClosest(int[] nums, int target) {
       
        Arrays.sort(nums);
        int rough=nums[0]+nums[1]+nums[2];
        int mindiff=Integer.MAX_VALUE;
 
        for(int i=0;i<nums.length-2;i++){
            int p=i+1,j=nums.length-1;
            while(p<j){

                int sum = nums[i]+nums[p]+nums[j];
                if(sum==target){
                    return target;
                    
                }else if(sum<target)
                    p++;
                else
                j--;

                int diff=Math.abs(sum-target);
                System.out.println(diff);
                if(diff<mindiff){
                    rough=sum;
                    mindiff=diff;
                }

            }
        }
        return rough;
    }
}