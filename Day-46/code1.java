class Solution
{
    // int fun(int arr[],int i){
        
    //     if(i>=arr.length)
    //         return 0;
    //     return Math.max(fun(arr,i+2)+arr[i],fun(arr,i+1));
    // }
    //Function to find the maximum money the thief can get.
    public int FindMaxSum(int arr[], int n)
    {
        
        // return fun(arr,0);
        // Your code here
        int arr1[]=new int[n];
        
        arr1[0]=arr[0];
        arr1[1]=Math.max(arr1[0],arr[1]);
        
        for(int i=2;i<arr.length;i++){
            arr1[i]=Math.max(arr1[i-2]+arr[i],arr1[i-1]);
        }
        
        return arr1[n-1];
    }
    
}