class Solution {
    public List<List<Integer>> subsets(int[] nums) {
       List<List<Integer>> al= new ArrayList<>();

       fun(al,new ArrayList<>(),nums,0);

       return al;
    }

    void fun(List<List<Integer>> al,List<Integer> all,int arr[],int i){

        al.add(new ArrayList<>(all));
        System.out.println(all);
        for(int k=i;k<arr.length;k++){
            all.add(arr[k]);
        
            fun(al,all,arr,k+1);

            all.remove(all.size()-1);
        }
    }
}