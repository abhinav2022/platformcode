class Solution {
    public int[] productExceptSelf(int[] nums) {
        int arr1[] = new int[nums.length];
    int left=1;
        for(int i=0;i<nums.length;i++){
            arr1[i]=left;
            left*=nums[i];
        }
        int right=1;

        for(int i=nums.length-1;i>=0;i--){
            arr1[i]*=right;
            right*=nums[i];
        }

        return arr1;
    }
}