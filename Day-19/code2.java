class Solution {
    public long zeroFilledSubarray(int[] nums) {
        long cnt=0;
        long val=0;

        for(int i=0;i<nums.length;i++){
            if(nums[i]==0){
                cnt++;
            }else if(nums[i]!=0 && cnt!=0){
                val+=(cnt*(cnt+1))/2;   //using sum of n numbers
                cnt=0;
            }
        }

        val+=(cnt*(cnt+1))/2;

        return val;
    }
}