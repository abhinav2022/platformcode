class Solution {
    public int maxArea(int[] arr) {
        
        int max=Integer.MIN_VALUE,i=0,j=arr.length-1;
       
       while(i<j){
        //    System.out.println("i= "+i);
           if(arr[i]>arr[j]){
            
               int l=arr[j]*(j-i);
               if(max<l)
               max=l;

               j--;
           }else{
               int l=arr[i]*(j-i);
               if(max<l)
               max=l;

               i++;
           }
           
       }

        return max;
    }
}