
class Solution{
    
    // arr: input array
    // n: size of array
    // Function to find the trapped water between the blocks.
    static long trappingWater(int arr[], int n) { 
        // Your code here
        if(arr.length<=2)
            return 0;
            
        long trap=0;
        
        int arr1[]=new int[arr.length];
        int arr2[]=new int[arr.length];
        
        int min=Integer.MIN_VALUE;
        for(int i=0;i<arr.length;i++){
            if(min<arr[i])
                min=arr[i];
                
                arr1[i]=min;
        }
        min=Integer.MIN_VALUE;
        for(int i=arr.length-1;i>=0;i--){
            if(min<arr[i])
                min=arr[i];
                
                arr2[i]=min;
        }
        
        for(int i=1;i<arr.length-1;i++){
            int m=Math.min(arr1[i],arr2[i]);
            
            trap+=(m-arr[i]);
        }
        
        return trap;
    } 
}


