
class Solution {
    void rotateMatrix(int arr[][], int n) {
        // code here
        for(int i=0;i<arr.length;i++){
            int j=0,k=arr[i].length-1;
            
            while(j<k){
                int temp=arr[i][j];
                arr[i][j]=arr[i][k];
                arr[i][k]=temp;
                j++;
                k--;
            }
        }
        
        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr[i].length;j++){
                int temp = arr[i][j];
                arr[i][j]=arr[j][i];
                arr[j][i]=temp;
            }
        }
    }
}