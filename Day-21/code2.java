class Solution {
    public int maxAreaOfIsland(int[][] grid) {
        int area=0;
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[i].length;j++){
                if(grid[i][j]==1){
                    area=Math.max(area,find(grid,i,j));
                }
            }
        }

        return area;
    }

    public int find(int grid[][],int i,int j){
            int y=1;
        if(i<0||j<0  || i>=grid.length || j>=grid[i].length || grid[i][j]==0 )
            return 0;

       grid[i][j]=0;// for allowing not to visit the same patch

        return 1+find(grid,i,j+1)+find(grid,i-1,j)+find(grid,i+1,j)+find(grid,i,j-1); //recursion for travesing all sides
    }
}