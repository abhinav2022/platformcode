class Solution
{
    int findMaxDiff(int a[], int n)
    {
	// Your code here	
	        int left[]=new int[n];
	        int right[]=new int[n];
	        
	     
	        for(int i=1;i<n;i++){
	          
	            for(int j=i-1;j>=0;j--){
	                if(a[i]>a[j]){
	                    left[i]=a[j];
	                    break;
	                }
	            }
	        }
	        
	        for(int i=0;i<n;i++){
	            right[i]=0;
	            for(int j=i+1;j<n;j++){
	                if(a[i]>a[j]){
	                    right[i]=a[j];
	                    break;
	                }
	            }
	        }
	        /* System.out.println();
	        for(int i=0;i<n;i++){
	            System.out.println(left[i]+" "+right[i]);
	        }
	        System.out.println();*/
	        int y=-1;
	        for(int i=0;i<n;i++){
	            y=Math.max(y,Math.abs(left[i]-right[i]));
	        }
	        
	        return y;
    }
}