class Solution {
    public int eraseOverlapIntervals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> a[1] - b[1]);

        int count = 1;
        int end = intervals[0][1];
        // for(int i=0;i<intervals.length;i++){
        //     for(int j=0;j<intervals[i].length;j++){
        //         System.out.print(intervals[i][j]+" ");
        //     }
        //     System.out.println();
        // }
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] >= end) {
                count++;
                end = intervals[i][1];
            }
        }

        return intervals.length - count;
    }
}