class Solution {
    public int[][] merge(int[][] intervals) {
       // int arr[][]=new int[intervals.length][];
       Arrays.sort(intervals,(arr,brr)-> Integer.compare(arr[0],brr[0]));

       ArrayList<int[]> al = new ArrayList<int[]>();

       int [] carr = intervals[0];

        al.add(carr);
       for(int [] interval:intervals){
            int cur_beg=carr[0];
            int cur_end=carr[1];
            int next_beg=interval[0];
            int next_end=interval[1];

            if(cur_end>=next_beg){
                carr[1]=Math.max(cur_end,next_end);
            }else{
                carr=interval;
                al.add(carr);
            }
       }

       return al.toArray(new int[al.size()][]);
    }
}