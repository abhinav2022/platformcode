class Solution {
    public int subarraysDivByK(int[] nums, int k) {
        int cnt=0;
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        int pre=0;
        // for(int i=0;i<k;i++)
        // hm.put(i,1);
        hm.put(0,1);
        for(int i=0;i<nums.length;i++){
            pre+=nums[i];
            int y=pre%k;
            if(y<0)
                y+=k;
            if(hm.containsKey(y) ){
                cnt+=hm.get(y);
            }
            hm.put(y,hm.getOrDefault(y,0)+1);
        }
       

        return cnt;
    }
}