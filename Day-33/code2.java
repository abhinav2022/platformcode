

class Solution {
    
    public long countSubArrayProductLessThanK(long a[], int n, long k)
    {
        long p=1,cnt=0;
        int j=0;
        for(int i=0;i<a.length;i++){
           
           p*=a[i];
           
           while(i>=j && k<=p){
               
               p=p/a[j];
               j++;
           }
           
           cnt+=(i-j)+1;
        }
        
        return cnt;
    }
}