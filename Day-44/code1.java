import java.util.*;
class Solution {

    class Transactions{

        int time;
        int price;
        String place;
        String name;

        Transactions(String name,int time,int price,String place){
            this.name=name;
            this.time=time;
            this.price=price;
            this.place=place;
        }
    }

    public List<String> invalidTransactions(String[] transactions) {
          List<String> al = new ArrayList<String>();

            HashMap<String,List<Transactions>> hm= new HashMap<>();

            for(int i=0;i<transactions.length;i++){
                String val[]= transactions[i].split(",");
                Transactions t = new Transactions(val[0],Integer.parseInt(val[1]),Integer.parseInt(val[2]),val[3]);
                hm.putIfAbsent(t.name,new ArrayList<>());
                hm.get(t.name).add(t);
            }

        for(int i=0;i<transactions.length;i++){
            String []val = transactions[i].split(",");
            Transactions t = new Transactions(val[0],Integer.parseInt(val[1]),Integer.parseInt(val[2]),val[3]);

            if(fun(t,hm.getOrDefault(t.name,new ArrayList<>())))
                al.add(transactions[i]);
        }

        return al;
    }

    boolean fun(Transactions t, List<Transactions> l){
        if(t.price>1000)
            return true;

            for(Transactions tr:l){
                if(Math.abs(tr.time - t.time)<=60 && !tr.place.equals(t.place))
                    return true;
            }

            return false;
    }
}