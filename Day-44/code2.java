import java.util.* ;
import java.io.*; 
import java.util.ArrayList;

public class Solution {

    public static int subArrayCount(ArrayList < Integer > arr, int k) {

    	 int cnt=0;
        HashMap<Long,Long> hm = new HashMap<>();
        int l=arr.size();
        long pre=0,u=0;
        // for(int i=0;i<k;i++)
        // hm.put(i,1);
        hm.put(pre,pre+1);
        for(int i=0;i<l;i++){
            pre+=arr.get(i);
            long y=(pre)%k;
            if(y<0)
                y+=k;
            if(hm.containsKey(y) )
                cnt+=hm.get(y);
            
            hm.put(y,hm.getOrDefault(y,u)+1);
        }
       

        return cnt;
    }
    
}