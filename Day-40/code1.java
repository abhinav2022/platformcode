class Solution {
    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        long mod=(long)1e9+7;
        long x=Math.max(horizontalCuts[0],h-horizontalCuts[horizontalCuts.length-1]);
        long y=Math.max(verticalCuts[0],w-verticalCuts[verticalCuts.length-1]);

        for(int i=1;i<horizontalCuts.length;i++){
            x=Math.max(x,(long)(horizontalCuts[i]-horizontalCuts[i-1]));
        }
        for(int i=1;i<verticalCuts.length;i++){
            y=Math.max(y,(long)(verticalCuts[i]-verticalCuts[i-1]));
        }

        return (int)((x*y)%mod);
    }
}