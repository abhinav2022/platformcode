class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        int arr[]=new int[nums.length-k+1];
        int u=0;
        Deque<Integer> dq = new ArrayDeque<Integer>();
        if(k==1){
            for(int i=0;i<nums.length;i++){
                arr[i]=nums[i];
            }

            return arr;
        }
        int i=0;
        for( i=0;i<k;i++){

            while(!dq.isEmpty() && nums[i]>=nums[dq.peekLast()])
                dq.removeLast();

                dq.addLast(i);
        }

        for(;i<nums.length;i++){

            arr[u++]=nums[dq.peek()];

            while((!dq.isEmpty()) && dq.peek()<=i-k)
            dq.removeFirst();

            while((!dq.isEmpty())&& nums[i]>=nums[dq.peekLast()])
                dq.removeLast();

            dq.addLast(i);
        }

        arr[u]=nums[dq.peek()];
        return arr;
    }
}