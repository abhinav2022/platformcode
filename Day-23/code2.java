class Solution {
    public int numPairsDivisibleBy60(int[] time) {
        int cnt=0;

        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();

        for(int i=0;i<time.length;i++){
            int y=time[i]%60;
            if(!hm.containsKey(y)){                
                    
                     if(hm.containsKey(60-y)){
                            cnt+=hm.get(60-y);
                     }
                      hm.put(y,1);
            }else{
                     if((hm.containsKey(60-y)))
                        cnt+=hm.get(60-y);
                    else if(y==0)
                        cnt+=hm.get(y);
                    

                    hm.put(y,hm.getOrDefault(y,0)+1);
            }
        }
        return cnt;
    }
}