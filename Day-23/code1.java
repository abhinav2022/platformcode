class Solution {
    public void gameOfLife(int[][] arr) {
        int arr2[][]=new int[arr.length][arr[0].length];
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                arr2[i][j]=arr[i][j];
            }
        }
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[i].length;j++){
                int y=fun1(arr2,i,j);
                if(arr2[i][j]==1 && (y<2 ||y>3)){
                    arr[i][j]=0;
                }else if(arr2[i][j]==0 && (y==3)){
                    arr[i][j]=1;
                }
            }
        }
    }

    int fun1(int arr[][],int i,int j){

        return fun(arr,i,j-1)+fun(arr,i,j+1)+fun(arr,i-1,j-1)+fun(arr,i-1,j)+fun(arr,i+1,j)+fun(arr,i-1,j+1)+fun(arr,i+1,j+1)+fun(arr,i+1,j-1);
    }
    int fun(int [][]arr,int i,int j){
        if(i<0||j<0 || i>=arr.length||j>=arr[i].length)
            return 0;
        
        else
            return arr[i][j];
    }
}