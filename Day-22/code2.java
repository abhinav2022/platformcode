class Solution {
    public int subarraySum(int[] nums, int k) {
        int pre=0,cnt=0;
        HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();
        hm.put(0,1);
        for(int i=0;i<nums.length;i++){
            pre+=nums[i];
            if(hm.containsKey(pre-k))
                cnt+=hm.get(pre-k);

            hm.put(pre,hm.getOrDefault(pre,0)+1);
        }


        return cnt;

    }
}