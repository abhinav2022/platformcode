User function Template for Java

class Solution
{
    // arr[]: Input Array
    // N : Size of the Array arr[]
    //Function to count inversions in the array.
    static long sort(long arr[],int start,int mid,int end){
        
        int r1=mid-start+1;
        int r2=end-mid;
        
        long arr1[] = new long[r1];
        long arr2[] = new long[r2];
        int u=start;
        for(int i=0;i<arr1.length;i++){
            arr1[i]=arr[u++];
        }
        for(int i=0;i<arr2.length;i++){
            arr2[i]=arr[u++];
        }
        u=start;
        int i=0,j=0;
        long cnt=0;
        
        while(i<arr1.length && j<arr2.length){
            if(arr1[i]>arr2[j]){
                arr[u++]=arr2[j++];
               cnt+=(arr1.length-i);
            }else{
                arr[u++]=arr1[i++];
            }
        }
        
        while(i<arr1.length){
            arr[u++]=arr1[i++];
        }
        
        while(j<arr2.length){
            arr[u++]=arr2[j++];
        }
        return cnt;
    }
    static long msort(long arr[],int start,int end){
        long cnt=0;
        if(start<end){
            int mid=(start+end)/2;
            cnt+=msort(arr,start,mid);
            cnt+=msort(arr,mid+1,end);
           cnt+= sort(arr,start,mid,end);
        }
        
        return cnt;
    }
    static long inversionCount(long arr[], long N)
    {
       return msort(arr,0,arr.length-1);
    }
}