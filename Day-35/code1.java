class Solution {
    public void nextPermutation(int[] nums) {
        int u=0,l=0;
        for( l=nums.length-2;l>=0;l--){
            if(nums[l+1]>nums[l]){
                u=l;
                break;
            }
        }
        if(l==-1){
            int i=0,j=nums.length-1;
             while(i<j){
                 int temp=nums[i];
                 nums[i]=nums[j];
                 nums[j]=temp;
                 i++;
                 j--;
             }

             return;
        }
        int y=0;
        for(int i=nums.length-1;i>u;i--){
            if(nums[i]>nums[u]){
                y=i;
                break;
            }
        }

        int temp =nums[u];
        nums[u]=nums[y];
        nums[y]=temp;
        u++;
    y=nums.length-1;
        while(u<y){
             int tem=nums[u];
                 nums[u]=nums[y];
                 nums[y]=tem;
                 u++;
                 y--;
        }
        return;
    }
}