class Solution {
    public void rotate(int[] nums, int k) {
        if(k==nums.length || nums.length==1)
        return;
        k=k%nums.length;
        
        int i=0,j=nums.length-1;
        fun(i,j,nums);
        fun(0,k-1,nums);
        fun(k,nums.length-1,nums);
                  
        }

     void fun(int i,int j,int nums[]){
             while(i<j){
            int temp=nums[i];
            nums[i]=nums[j];
            nums[j]=temp;
            i++;
            j--;
        }
     }
}