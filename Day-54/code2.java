class GfG{
  /*You are required to complete this method */
   public long multiplyTwoLists(Node l1,Node l2){
          //add code here.
          long x1=0;
          
          Node temp1=l1;
          
          while(temp1!=null){
              x1=(long)(temp1.data+x1*10)%(1000000007);
              temp1=temp1.next;
          }
          
          long x2=0;
          temp1=l2;
          
          while(temp1!=null){
              x2=(long)(x2*10+temp1.data)%(1000000007);
              temp1=temp1.next;
          }
          
          return (x2*x1)%(1000000007);
   }
}